#!/bin/sh
rm -rf /tmp/.X11-unix /tmp/.X0-lock /tmp/tuner.log
rm -rf /root/.vnc
mkdir /root/.vnc
chmod 0600 /root/.vnc

if [ -z "${TUNER_CONTROL_PASSWORD}" ]; then
    echo "No password specified, generating one"
    GENERATED_PASS=$(xkcdpass)
    TUNER_CONTROL_PASSWORD=$(echo "${GENERATED_PASS}" | sed "s/ //g")
    echo "The full control password is: \"${GENERATED_PASS}\" (without spaces)"
fi

cat <<EOF | vncpasswd -f > /root/.vnc/passwd
${TUNER_CONTROL_PASSWORD}
viewpass
EOF

jackd -d dummy -r 44100 -C 0 -P 0 &
cd /root/
Xvnc :0 -geometry 1440x900 -depth 16 \
    -rfbauth "/root/.vnc/passwd" &
# Wait for Xvnc to become available
while true; do
    sleep 1
    [ -S /tmp/.X11-unix/X0 ] && break
done

export DISPLAY=:0
openbsd-cwm &
(python3 /root/fmdemod_qt.py 2>&1 | tee /tmp/tuner.log) &
# Attempt to maximize the window across the entire desktop
while true; do
    wmctrl -r ":ACTIVE:" -b add,maximized_vert,maximized_horz && break || :
done
darkice &
# If the tuner goes haywire (evident in lots of underrun errors),
# exit and let the container runtime restart it
while true; do
    grep "jUjUjUjUjUjUjUjUjU" /tmp/tuner.log && break
    sleep 10
done