# Ten64 Software Defined Radio Demo - remote FM radio tuner

![Header image - Ten64 and LimeSDR](/frontend/html/title-image.jpg)

This is a simple demonstration of how to use a software defined radio (e.g LimeSDR)
with a Ten64.

The demonstration uses a demodulator built in GNURadio to stream audio out to the
internet via Icecast. A VNC server is presented over websockets to display
the visualizations (RF spectrum, waterfall, audio spectrum).

The GNURadio flowgraph is from the [GNURadio wiki](https://wiki.gnuradio.org/index.php/FM_Demod).

Deployment is via a docker-compose stack.

## Screenshot
![Screenshot](screenshot.png)

## Deployment
You will need a working Docker installation and a docker-compose client.

This example can be deployed on "bare-metal" or under a VM with LimeSDR operating as a USB passthrough device.

At least two CPU cores are required - the GUI is affined to the second CPU core, and the radio chain on the first.

To build and deploy:
```
# If you are deploying against a remote docker server
export DOCKER_HOST=ten64:2375

docker-compose -p sdrfm build

docker-compose -p sdrfm up -d
```

Go to "http://yourdockerhost:8000/" for the web interface.

The radio stream can be played directly from a client such as VLC or mplayer 
by using the "http://yourdockerhost:8000/sdrfm" URL.

By default, the web page will connect to the VNC server in view only mode, and a password is required
to get full control access to the VNC server and tuner.

You can change the administrator password by modifying the `TUNER_CONTROL_PASSWORD` variable in the
docker-compose stack.

## Hardware notes
* Even at a relatively low sample rate, a "bare" LimeSDR gets warm enough that it will become unstable
and lose connection with GNURadio after 30 minutes - 1 hour.

    I can highly recommend the [Lime AC Case](https://www.crowdsupply.com/hackergadgets/lime-ac-case) - after
installing my LimeSDR board I was able to run the server uninterrupted for four days straight.

* I found the LimeSDR is not as sensitive as hardware like the 
    [RTL-SDR](https://www.rtl-sdr.com/buy-rtl-sdr-dvb-t-dongles/) - 
    which uses hardware that was designed for FM/VHF in the first instance.

    If you are like me and only have cellular and WiFi SMA antennas around - 
    they will work but are not very sensitive at FM frequencies - 
    even if you have line of sight to the FM transmitter you are tuning into.

    I connected my LimeSDR to my free-to-air TV antenna, using an SMA cable->`SMA-BNC`->`BNC-F connector` and got the best results that way.

## Future improvements
* Find a better way to stream frequency/FFT/waterfall to the browser

   I tried [gr-bokehgui](https://github.com/gnuradio/gr-bokehgui), but the server side process was chewing up
   massive amounts of CPU on my Ryzen based desktop - and in the browser. Using Xvnc + websockets + noVNC embedding
   is significantly less resource intensive on both the server and client!

* Stereo FM decoding - adapting the [fm-rds decoder](https://github.com/alexmrqt/fm-rds)

* Slim down the tuner image, possibly rebasing on buildroot.

* Make the tuner work as an unprivledged user

* "Edge computing" edition - deploy on Kubernetes with the Icecast server in the "cloud" and multiple tuner instances in the
field.